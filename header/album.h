#ifndef ALBUM_H
#define ALBUM_H

#include <iostream>
#include <string>
#include <ctime>

/** \class Album
 */
class Album
{

  //Attributs

 private:
  int id; ///< Object identifier
  std::tm date; ///< Album release date
  std::string name; ///< Artist name string



 public:

  //méthodes
  //Constructeurs/destructeur

/** \brief Default constructor
   *
   * With blank #id, #date and #name
   */
  Album();

/**
*
*@param 
*/



 /** \brief Detailed constructor
   *
   * Construct an #Album object based on the detailed parameterization
   *
   * \param[in] _id The identification number
   *
   * \param[in] _date The release date
   *
   * \param[in] _name The #Album's name string
   */

Album(int, std::tm _date, std::string);

  /**
    *
    * \brief Destructor
    */
  ~Album();

 /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  int getId();

  /** \brief Get the Album release date
   *
   * \return The Album release date
   */

  std::tm getDate();

/** \brief Get the name of the Album
   *
   * \return The Album name
   */

  std::string getName();
 /** \brief Set the Album's release date
   *
   * \param[in] _date The release date
   */

  void setDate(std::tm _date);

   /** \brief Set the name of the Album
   *
   * \param[in] _name The name to set
   */

  void setName(std::string);


};

#endif //ALBUM_H
