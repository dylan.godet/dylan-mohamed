#ifndef POLYPHONY_H
#define POLYPHONY_H

#include <iostream>
#include <string>

/** \class Polyphony
 *
*/
class Polyphony
{

  // Attributs

 private:
  int id;///< Object identifier
  int number;///< Polyphony channel number

 public:


  // Méthodes
  // Constructeurs

/** \brief Default constructor
   *
   * With null #id and #number
   */

  Polyphony();

  /** \brief Detailed constructor
   *
   * Construct a #Polyphony object based on the detailed
   * parameterization
   *
   * \param[in] _id The #Polyphony identification number
   *
   * \param[in] _number The #Polyphony channel number
   */

  Polyphony(int, int);

  /** \brief destructeur
  */

  ~Polyphony();
 /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */

  int getId();

  /** \brief Get the object's #number
   *
   * \return A copy of the #number
   */

  int getNumber();

    /** \brief Set the object's #number
   *
   * \param[in] _number The channel #number to set
   */

  void setNumber(int);

};


#endif //POLYPHONY_H

