#ifndef FORMAT_H
#define FORMAT_H

#include <iostream>
#include <string>

/** \class Format
 *
*/

class Format
{
  //Propriétés
 private:
  int id;///< Object identifier
  std::string libelle;///< Format description string



 public:


  //méthodes

  //Constructeurs
  //Constructeurs/destructeur

 /** \brief Default constructor
   *
   * With null #id and #description
   */

  Format();

/** \brief Detailed constructor
   *
   * Construct a #Format object based on the detailed parameterization
   *
   * \param[in] _id The identification number
   *
   * \param[in] _description The #Format's description string
   */

  Format(unsigned int, std::string);

  /** \brief destructeur
  */
  ~Format();


/** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */

  int getId();

/** \brief Get the object's #description
   *
   * \return A copy of the #description \c std::string
   */

  std::string getLibelle();

/** \brief Set the object's #description
   *
   * \param[in] _description The \c std::string to use as #description
   */

  void setLibelle(std::string);




};
#endif //FORMAT_H
